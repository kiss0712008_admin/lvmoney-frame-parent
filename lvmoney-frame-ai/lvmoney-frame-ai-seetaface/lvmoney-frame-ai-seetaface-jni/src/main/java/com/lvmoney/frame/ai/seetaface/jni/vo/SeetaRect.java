package com.lvmoney.frame.ai.seetaface.jni.vo;/**
 * 描述:
 * 包名:com.lvmoney.frame.member.info
 * 版本信息: 版本1.0
 * 日期:2020/1/20
 * Copyright XXXXXX科技有限公司
 */

/**
 * @describe：
 * @author: lvmoney /XXXXXX科技有限公司
 * @version:v1.0 2020/1/20 14:08
 */
public class SeetaRect {

    /**
     * 左上角点横坐标
     */
    public int x;
    /**
     * 左上角点纵坐标
     */
    public int y;
    /**
     * 矩形宽度
     */
    public int width;
    /**
     * 矩形高度
     */
    public int height;

}
